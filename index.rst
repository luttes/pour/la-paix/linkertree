

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

.. un·e
.. ❤️💛💚

.. https://framapiaf.org/web/tags/viviansilver.rss
.. https://framapiaf.org/web/tags/womenwagepeace.rss
.. https://framapiaf.org/web/tags/parentscirclefriends.rss

.. _paix_linkertree:

===============================================================================
|paix| 💚 **Liens sur la paix** |vivian|
===============================================================================


Rav Floriane Chinksy
=======================

- https://judaism.gitlab.io/floriane_chinsky


Guerrières de la paix
========================

- https://luttes.frama.io/pour/la-paix/les-guerrieres-de-la-paix/


Parentscirclefriends
======================

- https://luttes.frama.io/pour/la-paix/parentscirclefriends


La Paix maintenant
======================

- https://www.lapaixmaintenant.org/

**Standing together (omdimbeyachad)**
===============================================================================

- https://www.standing-together.org/about-us
- https://www.instagram.com/standing.together.movement/
- https://www.instagram.com/standing.together.english/
- https://www.youtube.com/@standingtogether7105/videos
- https://leftodon.social/@omdimbeyachad

Women Wage Peace
==================

- https://luttes.frama.io/pour/la-paix/womenwagepeace

Women of the Sun
======================

- https://en.wikipedia.org/wiki/Women_of_the_Sun_(movement)
